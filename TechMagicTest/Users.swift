//
//  Users.swift
//  TechMagicTest
//
//  Created by Khrystyna Shevchuk on 5/6/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import Foundation
import JSONJoy

class Users: JSONJoy {
    
    var users = [User]()
    
    required init(_ decoder: JSONDecoder) {
        if let users = decoder.array {
            for user in users {
                self.users.append(User(user))
            }
        }
    }
}

class User: JSONJoy {
    
    var login: String
    var id: Int
    var avatarUrl: String
    var gravatarId: String
    var url: String
    var htmlUrl: String
    var followersUrl: String
    var followingUrl: String
    var gistsUrl: String
    var starredUrl: String
    var subscriptionsUrl: String
    var organizationsUrl: String
    var reposUrl: String
    var eventsUrl: String
    var receivedEventsUrl: String
    var type: String
    var siteAdmin: Bool
    
    required init(_ decoder: JSONDecoder) {
        login = decoder["login"].string ?? ""
        id = decoder["id"].integer ?? Int()
        avatarUrl = decoder["avatar_url"].string ?? ""
        gravatarId = decoder["gravatar_id"].string ?? ""
        url = decoder["url"].string ?? ""
        htmlUrl = decoder["html_url"].string ?? ""
        followersUrl = decoder["followers_url"].string ?? ""
        followingUrl = decoder["following_url"].string ?? ""
        gistsUrl = decoder["gists_url"].string ?? ""
        starredUrl = decoder["starred_url"].string ?? ""
        subscriptionsUrl = decoder["subscriptions_url"].string ?? ""
        organizationsUrl = decoder["organizations_url"].string ?? ""
        reposUrl = decoder["repos_url"].string ?? ""
        eventsUrl = decoder["events_url"].string ?? ""
        receivedEventsUrl = decoder["received_events_url"].string ?? ""
        type = decoder["type"].string ?? ""
        siteAdmin = decoder["site_admin"].bool

    }
}