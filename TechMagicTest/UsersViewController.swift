//
//  ViewController.swift
//  TechMagicTest
//
//  Created by Khrystyna Shevchuk on 5/6/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

private let SEGUE = "urlSegue"

class UsersViewController: UIViewController {
    
    //MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: private vars
    private var users: Users?
    private var selectedUser: User?
    
    
    //MARK: view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUsers()
    }
    
    private func loadUsers() {
        DataProvider.loadUsers { (users, error) in
            if let unwrappedData = users {
                self.users = unwrappedData
                self.tableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension UsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users?.users.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UserCell
        
        let user = users?.users[indexPath.row]
        
        cell.iconImageView.image = UIImage(named: "image")
        if let avatarUrl = user?.avatarUrl {
            DataProvider.loadImageWithUrl(avatarUrl, completion: { (image, error) -> Void in
                if let unwrappedImage = image {
                    cell.iconImageView.image = unwrappedImage
                }
            })
        }
        
        cell.nameLabel.text = user?.login
        cell.urlLabel.text = user?.htmlUrl
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    
    //MARK: navigation
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedUser = users?.users[indexPath.row]
        performSegueWithIdentifier(SEGUE, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEGUE {
            if let vc = segue.destinationViewController as? WebViewController {
                vc.user = self.selectedUser
            }
        }
    }
}