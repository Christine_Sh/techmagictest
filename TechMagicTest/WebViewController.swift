//
//  WebViewController.swift
//  TechMagicTest
//
//  Created by Khrystyna Shevchuk on 5/6/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    //MARK: outlets
    @IBOutlet weak var webView: UIWebView!
    
    
    //MARK: vars
    var user: User? //this should be set before show(push) WebViewController

    //MARK: vc life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = user?.login
        
        load()
    }
    
    private func load() {
        if let urlString = user?.htmlUrl {
            if let url = NSURL(string: urlString) {
                webView.loadRequest(NSURLRequest(URL: url))
            }
        }
    }
}