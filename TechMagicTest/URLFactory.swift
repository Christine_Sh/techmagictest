//
//  URLFactory.swift
//  TechMagicTest
//
//  Created by Khrystyna Shevchuk on 5/6/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import Foundation

private let BASE_URL = "https://api.github.com"


///URLFactory is responsible for creating urls
struct URLFactory {
    
    static func urlGetUsers() -> String {
        return "\(BASE_URL)/users"
    }
}