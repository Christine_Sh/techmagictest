//
//  NetworkFetcher.swift
//  TechMagicTest
//
//  Created by Khrystyna Shevchuk on 5/6/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import Foundation

///NetworkFetcher is responsible for executing requests and logging each request/response pair
struct NetworkFetcher {
    
    static func executeURL(url: String, completion: (data:NSData?, response: NSURLResponse?, error: NSError?) -> Void) {
        
        if let url = NSURL(string: url) {
            executeURl(url, completion: completion)
            
        } else {
            completion(data: nil, response: nil, error: NSError(domain: "Parsing error", code: 000, userInfo: nil))
        }
    }
    
    static func executeURl(url: NSURL, completion: (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void) {
        executeRequest(NSURLRequest(URL: url), completion: completion)
    }
    
    static func executeRequest(request: NSURLRequest, completion: (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void) {
        NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) in
            
            self.logRequest(request, data: data, response: response, error: error)
            completion(data: data, response: response, error: error)
            
        }).resume()
    }
    
    
    private static func logRequest (request: NSURLRequest, data: NSData?, response: NSURLResponse?, error: NSError?) {
        
        if let unwrappedData = data{
            
            if let json = try? NSJSONSerialization.JSONObjectWithData(unwrappedData, options: NSJSONReadingOptions.AllowFragments) {
                print("response data : json\n \(json)")
            } else {
                
                let string = NSString(data: unwrappedData, encoding: NSUTF8StringEncoding)
                print("response data : string\n \(string)")
            }
        } else {
            print("response data == nil")
        }
        
        print("response - \(response)")
        print("error - \(error)")
    }
}