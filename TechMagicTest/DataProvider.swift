//
//  DataProvider.swift
//  TechMagicTest
//
//  Created by Khrystyna Shevchuk on 5/6/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import Foundation
import JSONJoy

/// DataProvider is responsible for fetching all data from service
struct DataProvider {
    
    static func loadUsers(completion: (users: Users?, error: NSError?)-> Void) {
        let url = URLFactory.urlGetUsers()
        NetworkFetcher.executeURL(url) { (data, response, error) in
            
            if let unwrappedError = error {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(users: nil, error: unwrappedError)
                })
                return
            }
            let users = Users(JSONDecoder(data!))
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(users: users, error: nil)
            })
        }
    }
    
    static func loadImageWithUrl(url: String, completion: (image: UIImage?, error: NSError?) -> Void) {
        
        NetworkFetcher.executeURL(url) { (data, response, error) -> Void in
            if let unwrappedData = data {
                if let image = UIImage(data: unwrappedData) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(image: image, error: nil)
                    })
                    return
                }
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(image: nil, error: error)
            })
        }
    }

}

